module.exports = {
  lintOnSave: false,
  devServer: {
    proxy: {
      "/api*": {
        target: "http://localhost:3000",
        secure: false,
      }
    }
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/styles/_mixins";
          @import "@/styles/_variables";
        `
      }
    }
  }
};
