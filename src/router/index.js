import Vue from 'vue';
import VueRouter from 'vue-router';

const MainPage = () => import('@/views/site/MainPage');
const DirectionPage = () => import('@/views/site/DirectionPage');
const CountryPage = () => import('@/views/site/CountryPage');
const ContactsPage = () => import('@/views/site/ContactsPage');
const AdminPage = () => import('@/views/admin/AdminPage');
const LoginPage = () => import('@/views/admin/LoginPage');

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: MainPage,
  },
  {
    path: '/direction/:direction',
    component: DirectionPage,
  },
  {
    path: '/country/:country',
    component: CountryPage,
  },
  {
    path: '/contacts',
    component: ContactsPage,
  },
  {
    path: '/admin',
    component: { render: h => h('router-view') },
    children: [
      { path: 'login', component: LoginPage },
      { path: 'main', component: AdminPage },
      { path: '', redirect: 'main' },
      { path: '*', redirect: 'main' }
    ]
  },
  {
    path: '*',
    redirect: '/'
  }
];

const scrollBehavior = () => {
  return { x: 0, y: 0 };
};

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior,
  routes
});

export default router;
