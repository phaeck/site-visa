import Vue from 'vue';
import Vuex from 'vuex';
import Documents from './modules/documents';
import AppData from './modules/app-data';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    Documents,
    AppData,
  }
});
