import { DIRECTIONS } from '@/core/constants';

export default {
  state: {
    directions: DIRECTIONS,
  },
  getters: {
    directions(state) {
      return state.directions;
    },
    countries(state) {
      return state.directions.map(({countries}) => countries).flat();
    }
  }
}
