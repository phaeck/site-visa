import {DOCUMENT_LIST} from '@/core/constants/document-list';

const defaultDocumentsList = DOCUMENT_LIST;

export default {
  actions: {
    setDocuments({commit}, documentNames) {
      const documents = defaultDocumentsList.filter(({name}) => documentNames.includes(name));
      commit('setRequiredDocuments', documents);
    },
    setDefaultDocuments({commit}) {
      commit('setRequiredDocuments', defaultDocumentsList);
    }
  },
  mutations: {
    setRequiredDocuments(state, documents) {
      state.requiredDocuments = documents
    }
  },
  state: {
    requiredDocuments: defaultDocumentsList,
  },
  getters: {
    requiredDocuments(state) {
      return state.requiredDocuments;
    }
  },
}
