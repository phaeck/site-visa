export const sendCallbackMixin = {
  methods: {
    sendCallbackRequest(data) {
      const url = '/api/request';
      return fetch(url, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data),
      })
    }
  }
};
