import Vue from 'vue';
import App from '@/App.vue';
import router from '@/router';
import store from '@/store';
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome';
import ClickOutsideDirective from '@/core/directives/click-outside-directive';
import VueCarousel from 'vue-carousel';

// directives
Vue.directive('click-outside', ClickOutsideDirective);

// external
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('font-awesome-layers', FontAwesomeLayers);
Vue.component('font-awesome-layers-text', FontAwesomeLayersText);
Vue.use(VueCarousel);

Vue.config.productionTip = false;

const root = new Vue({
  router,
  store,
  render: h => h(App)
});

document.addEventListener('DOMContentLoaded', () => root.$mount('#app'));
