export const DOCUMENT_LIST = [
  { id: 1, name: 'foreign', text: 'Заграничный паспорт', src: require('@/assets/zagran.jpg') },
  { id: 2, name: 'foreign-copy', text: 'Копии гражданского паспорта', src: require('@/assets/copy-grazhdan.jpg') },
  { id: 3, name: 'two-photos', text: '2 фотографии на матовой бумаге цветные (3,5 х 4,5 см)', src: require('@/assets/foto.png') },
  { id: 4, name: 'work', text: 'Справка с места работы', src: require('@/assets/letter_work.png') },
  { id: 5, name: 'bank', text: 'Выписка о состоянии счёта', src: require('@/assets/letter_bank.png') },
];
