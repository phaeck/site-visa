const shengenServices = [
  {
    title: 'Личная подача',
    subtitle: 'При наличии документов для поездки',
    price: '2 000 руб.',
    chargeText: 'Сборы оплачиваются отдельно: ≈ 4 700 руб.',
    serviceList: [
      'Проконсультируем',
      'Проверим все документы',
      'Сделаем необходимые копии',
      'Запишем на подачу и сдачу биометрии',
      'Напишем  спонсорские письма',
      'Заполним анкету на иностранном языке',
      'Заполним бланк обработки персональных данных',
      'Оформим страховку для получения визы',
    ],
    steps: [
      'Предоставить минимальный пакет документов и передать его любым удобным вам способом',
      'Предоставить подтверждение перелета и проживания',
      'Побывать в визовом отделе Консульства, оплатить сборы (≈ 4 700 руб.), сдать отпечатки пальцев',
      'Получить паспорт с визой',
    ],
  },
  {
    title: 'Личная подача',
    subtitle: null,
    price: '2 900 руб.',
    chargeText: 'Сборы оплачиваются отдельно: ≈ 4 700 руб.',
    serviceList: [
      'Проконсультируем',
      'Проверим все документы',
      'Сделаем необходимые копии',
      'Запишем на подачу и сдачу биометрии',
      'Напишем  спонсорские письма',
      'Заполним анкету на иностранном языке',
      'Предоставим ваучер от отеля с отметкой об оплате',
      'Заполним бланк обработки персональных данных',
      'Оформим страховку для получения визы',
    ],
    steps: [
      'Предоставить минимальный пакет документов и передать его любым удобным вам способом',
      'Побывать в визовом отделе Консульства, оплатить сборы (≈ 4 700 руб.), сдать отпечатки пальцев',
      'Получить паспорт с визой',
    ],
  },
  {
    title: 'Без присутствия',
    subtitle: null,
    price: '9000 руб.',
    chargeText: 'Все сборы включены в конечную стоимость',
    serviceList: [
      'Проконсультируем',
      'Проверим все документы',
      'Сделаем необходимые копии',
      'Запишем на подачу и сдачу биометрии',
      'Предоставим подтверждение перелета в обе стороны',
      'Составим сопроводительные и спонсорские письма',
      'Предоставим ваучер от отеля с отметкой об оплате',
      'Заполним анкету на иностранном языке',
      'Заполним бланк обработки персональных данных',
      'Оформим страховку для получения визы',
      'Сдадим ваши документы в Консульскую службу',
      'Оплатим за вас все сборы',
      'Получим ваш паспорт из консульства',
    ],
    steps: [
      'Предоставить минимальный пакет документов и передать его любым удобным вам способом',
      'Получить паспорт с визой',
    ],
  },
];

export const DIRECTIONS = [
  {
    id: 1,
    name: "Шенген",
    link: "shengen",
    directionTitle: "Шенгенская виза в Европу",
    countries: [
      {
        id: 1,
        name: "Бельгия",
        title: "Визы в Бельгию",
        img: require("@/assets/directions/shengen/Belgiya.png"),
        services: shengenServices,
        link: "belgium"
      },
      {
        id: 2,
        name: "Чехия",
        title: "Визы в Чехию",
        img: require("@/assets/directions/shengen/Czech.png"),
        services: shengenServices,
        link: "czech"
      },
      {
        id: 3,
        name: "Финляндия",
        title: "Визы в Финляндию",
        img: require("@/assets/directions/shengen/finland.png"),
        services: shengenServices,
        link: "finland"
      },
      {
        id: 4,
        name: "Латвия",
        title: "Визы в Латвию",
        img: require("@/assets/directions/shengen/latviya.png"),
        services: shengenServices,
        link: "latvia"
      },
      {
        id: 5,
        name: "Нидерланды",
        title: "Визы в Нидерланды",
        img: require("@/assets/directions/shengen/niderlandy.png"),
        services: shengenServices,
        link: "netherlands"
      },
      {
        id: 6,
        name: "Норвегия",
        title: "Визы в Норвегию",
        img: require("@/assets/directions/shengen/norway.png"),
        services: shengenServices,
        link: "norway"
      },
      {
        id: 7,
        name: "Польша",
        title: "Визы в Польшу",
        img: require("@/assets/directions/shengen/polan.png"),
        services: shengenServices,
        link: "poland"
      },
      {
        id: 8,
        name: "Швеция",
        title: "Визы в Швецию",
        img: require("@/assets/directions/shengen/Sweden.png"),
        services: shengenServices,
        link: "sweden"
      },
      {
        id: 9,
        name: "Швейцария",
        title: "Визы в Швейцарию",
        img: require("@/assets/directions/shengen/Switzerland.png"),
        services: shengenServices,
        link: "switzerland"
      },
      {
        id: 10,
        name: "Венгрия",
        title: "Визы в Венгрию",
        img: require("@/assets/directions/shengen/vengriya.png"),
        services: shengenServices,
        link: "hungary"
      }
    ]
  },
  {
    id: 2,
    name: "Европа",
    link: "europe",
    directionTitle: "Шенгенская виза в Европу",
    countries: [
      {
        id: 1,
        name: "Болгария",
        title: "Визы в Болгарию",
        subtitle: "Придумай что сюда написать для каждой страны",
        img: require("@/assets/directions/europe/bulgaria.png"),
        services: [
          { title: 'Мультивиза до 90 дней', subtitle: 'Для взрослых (старше 6 лет)', price: '5 500 руб.', chargeText: '5-7 рабочих дней' },
          { title: 'Срочная виза до 90 дней', subtitle: 'Для взрослых (старше 6 лет)', price: '8 500 руб.', chargeText: '2-3 рабочих дня' },
          { title: 'Мультивиза до 90 дней', subtitle: 'Для детей младше 6 лет', price: '2 000 руб.', chargeText: '5-7 рабочих дней' },
          { title: 'Срочная виза до 90 дней', subtitle: 'Для детей младше 6 лет', price: '3 000 руб.', chargeText: '2-3 рабочих дня' },
        ],
        link: "bulgaria",
        requiredDocuments: ['foreign', 'two-photo']
      },
      {
        id: 2,
        name: "Кипр",
        title: "Визы в Кипр",
        img: require("@/assets/directions/europe/cyprus.png"),
        services: [
          {
            title: 'PRO VISA',
            subtitle: null,
            price: '900 руб.',
            chargeText: 'Все включено',
            serviceList: [
              'Заполним анкету на иностранном языке',
              'Поможем оформить визу, даже если вы уже в аэропорту во время регистрации на рейс',
            ],
            steps: [
              'Прислать фото заграничного паспорта любым удобным способом и ответить на несколько вопросов',
              'Получить паспорт с визой',
            ],
          }
        ],
        link: "cyprus",
        requiredDocuments: ['foreign']
      },
      {
        id: 3,
        name: "Великобритания",
        title: "Визы в Великобританию",
        img: require("@/assets/directions/europe/GreatBritan.png"),
        services: [
          {
            title: 'Стоимость',
            subtitle: 'визы в Великобританию',
            price: '4 500 руб.',
            chargeText: 'Стоимость сборов зависит от срока визы',
            chargeList: [
              'до 6 месяцев: 133 USD',
              'до 2 лет: 468 USD',
              'до 5 лет: 849 USD',
              'до 10 лет: 1 150 USD',
              'за срочность: + 200 USD',
            ],
            serviceList: [
              'Проконсультируем',
              'Проверим документы',
              'Сделаем все необходимые копии',
              'Сделаем все необходимые переводы документов',
              'Запишем на подачу и сдачу биометрии',
              'Заполним анкету на иностранном языке',
              'Составим сопроводительные и спонсорские письма',
              'Заполним бланк обработки персональных данных',
              'Оформим страховку для получения визы',
              'Заберем паспорт с визой из визового отдела Консульства',
            ],
            steps: [
              'Собрать минимальный пакет документов и передать его любым удобным вам способом',
              'Побывать в визовом отделе Консульства, сдать биометрию',
              'Получить паспорт с визой',
            ],
          }
        ],
        link: "great-britain",
        requiredDocuments: ['foreign', 'foreign-copy', 'two-photo']
      },
      {
        id: 4,
        name: "Ирландия",
        title: "Визы в Ирландию",
        img: require("@/assets/directions/europe/Ireland.png"),
        services: [
          {
            title: 'Стоимость',
            subtitle: 'визы в Ирландию',
            price: '4 500 руб.',
            chargeText: 'Дополнительно оплачиваются сборы: 80 EUR',
            serviceList: [
              'Проконсультируем',
              'Проверим документы',
              'Сделаем все необходимые копии',
              'Сделаем все необходимые переводы документов',
              'Запишем на подачу и сдачу биометрии',
              'Заполним анкету на иностранном языке',
              'Составим сопроводительные и спонсорские письма',
              'Заполним бланк обработки персональных данных',
              'Оформим страховку для получения визы',
              'Сдадим ваши документы в Консульскую службу',
              'Оплатим за вас все сборы',
              'Заберем паспорт с визой из визового отдела Консульства',
            ],
            steps: [
              'Собрать минимальный пакет документов и передать его любым удобным вам способом',
              'Получить паспорт с визой',
            ],
          }
        ],
        link: "ireland",
        requiredDocuments: ['foreign', 'foreign-copy', 'two-photo']
      }
    ]
  },
  {
    id: 3,
    name: "Азия",
    link: "asia",
    directionTitle: "Виза в страны ближней и дальней Азии",
    countries: [
      {
        id: 1,
        name: "Китай",
        title: "Визы в Китай",
        img: require("@/assets/directions/asia/china.png"),
        link: "china"
      },
      {
        id: 2,
        name: "Индия",
        title: "Визы в Индию",
        img: require("@/assets/directions/asia/india.png"),
        link: "india"
      },
      {
        id: 3,
        name: "Япония",
        title: "Визы в Японию",
        img: require("@/assets/directions/asia/japan.png"),
        link: "japan"
      }
    ]
  },
  {
    id: 4,
    name: "Америка",
    link: "america",
    directionTitle: "Виза в страны Американского континента",
    countries: [
      {
        id: 1,
        name: "США",
        title: "Визы в США",
        img: require("@/assets/directions/america/usa.png"),
        link: "usa"
      },
      {
        id: 2,
        name: "Канада",
        title: "Визы в Канаду",
        img: require("@/assets/directions/america/canada.png"),
        link: "canada"
      }
    ]
  },
  {
    id: 5,
    name: "Австралия и Океания",
    link: "australia-oceanic",
    directionTitle: "Визы в страны Океании",
    countries: [
      {
        id: 1,
        name: "Австралия",
        title: "Визы в Австралию",
        img: require("@/assets/directions/australia-oceanic/Australia.png"),
        link: "australia"
      },
      {
        id: 2,
        name: "Новая Зеландия",
        title: "Визы в Новую Зеландию",
        img: require("@/assets/directions/australia-oceanic/newZealand.png"),
        link: "new-zealand"
      }
    ]
  }
];
