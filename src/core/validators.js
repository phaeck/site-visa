const PHONE_REGEX = /\+\d \(\d{3}\) \d{3}-\d{2}-\d{2}/;
const EMAIL_REGEX = /(.+)@(.+){2,}\.(.+){2,}/;

export const validatePhone = value => {
  return !!value && PHONE_REGEX.test(value);
};

export const validateName = value => {
  return value && value.length > 2;
};

export const validateEmail = value => {
  return !!value && EMAIL_REGEX.test(value);
};